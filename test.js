class Identite {
  constructor(name, age){
    this.name  = name;
    this.age   = age;
    this.utils = new Utils();
  }

  addToAge(...val){
    this.utils.addToAge.apply(this, val);
  }
}

class Utils {
    addToAge(...val){ val.forEach(v => {this.age+=v;}); }
}

let id = new Identite('Smith', 37);

id.addToAge(12, 2);

document.getElementById('name').textContent = id.name;
document.getElementById('age').textContent = id.age;
